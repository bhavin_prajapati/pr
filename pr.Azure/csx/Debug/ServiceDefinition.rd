﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="pr.Azure" generation="1" functional="0" release="0" Id="4b8770c5-89e1-4d46-863b-5c1737c69959" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="pr.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="pr:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/pr.Azure/pr.AzureGroup/LB:pr:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="prInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/pr.Azure/pr.AzureGroup/MapprInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:pr:Endpoint1">
          <toPorts>
            <inPortMoniker name="/pr.Azure/pr.AzureGroup/pr/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapprInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/pr.Azure/pr.AzureGroup/prInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="pr" generation="1" functional="0" release="0" software="C:\Users\Bhavin\Documents\Visual Studio 2013\Projects\pr\pr.Azure\csx\Debug\roles\pr" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;pr&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;pr&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/pr.Azure/pr.AzureGroup/prInstances" />
            <sCSPolicyUpdateDomainMoniker name="/pr.Azure/pr.AzureGroup/prUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/pr.Azure/pr.AzureGroup/prFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="prUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="prFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="prInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="b935e269-63a5-4190-9059-1cbbcefdb63c" ref="Microsoft.RedDog.Contract\ServiceContract\pr.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="9e535b56-8bc3-4177-b81b-3b9feebe3bc9" ref="Microsoft.RedDog.Contract\Interface\pr:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/pr.Azure/pr.AzureGroup/pr:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>