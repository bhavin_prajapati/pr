﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PR.Models;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using PR.Resources;
using Microsoft.Owin.Security.Facebook;

namespace PR.Controllers
{
    public class GamesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/Games
        [ResponseType(typeof(Game))]
        public async Task<IHttpActionResult> CreateGame([FromUri]GameViewModel gameViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var game = gameViewModel.CreateNewGame(db);
            return CreatedAtRoute("API Default", new { id = game.Id }, game);
        }
        
        public Game GetGamebyFacebookId(long fbId)
        {
            Game game = db.Games.Where(x => x.FacebookId == fbId).FirstOrDefault();
            return game;
        }

        public List<Ranking> GetRankings(Game game)
        {
            List<Ranking> rankings = db.Rankings.Where(x => x.SenderId == game.FacebookId).ToList();
            return rankings;
        }

        public async Task DeleteGame(long id)
        {
            Game game = await db.Games.FindAsync(id);
            db.Games.Remove(game);
            await db.SaveChangesAsync();
        }

        public async Task<bool> GameExists(long id)
        {
            return db.Games.Count(e => e.Id == id) > 0;
        }
    }
}