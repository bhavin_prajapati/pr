﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PR.Models;

namespace PR.Controllers
{
    public class RankingsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/Rankings
        [ResponseType(typeof(Ranking))]
        public async Task<IHttpActionResult> PostRanking(Ranking ranking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Rankings.Add(ranking);
            int _result = await db.SaveChangesAsync();
            return CreatedAtRoute("API Default", new { result = _result }, ranking);
        }
    }
}