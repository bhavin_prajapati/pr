﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PR.Models;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace PR.Controllers
{
    public class FacebookUsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public FacebookUser GetFacebookUserByFacebookId(long facebookId)
        {
            FacebookUser facebookUser = db.FacebookUsers.Where(x => x.FacebookId == facebookId).FirstOrDefault();
            return facebookUser;
        }

        public FacebookUser GetFacebookUserByEmail(string email)
        {
            FacebookUser facebookUser = db.FacebookUsers.Where(x => x.Email == email).FirstOrDefault();
            return facebookUser;
        }

        public async Task StoreFacebookUsers(List<FacebookUser> facebookUsers)
        {
            foreach (FacebookUser fbuser in facebookUsers)
            {
                if (!FacebookUserExistsByFacebookId(fbuser.FacebookId))
                {
                    db.FacebookUsers.Add(fbuser);
                }
            }
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

            }
        }

        public async Task UpdateFacebookUser(FacebookUser facebookUser)
        {
            FacebookUser updatedUser = db.FacebookUsers.Where(x => x.FacebookId == facebookUser.FacebookId).FirstOrDefault();
            if (updatedUser != null)
            {
                updatedUser.FirstName = facebookUser.FirstName;
                updatedUser.LastName = facebookUser.LastName;
                updatedUser.Gender = facebookUser.Gender;
                updatedUser.UpdatedTime = facebookUser.UpdatedTime;
                updatedUser.LoggedIn = facebookUser.LoggedIn;
            }

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

            }
        }
        
        private bool FacebookUserExists(int id)
        {
            return db.FacebookUsers.Count(e => e.Id == id) > 0;
        }

        private bool FacebookUserExistsByFacebookId(long facebookId)
        {
            return db.FacebookUsers.Count(e => e.FacebookId == facebookId) > 0;
        }
    }
}