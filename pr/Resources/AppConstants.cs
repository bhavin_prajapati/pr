﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR.Resources
{
    public class AppConstants
    {
        internal const String PRActorSystem = "PRActorSystem";
        internal const String AccessToken = "AccessToken";
        internal const String FacebookAuthenticatedContext = "FacebookAuthenticatedContext";
    }
}