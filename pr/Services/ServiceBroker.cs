﻿using System.Globalization;
using Microsoft.Practices.Unity;

namespace PR.Services
{
    public static class ServiceBroker
    {
        internal const string FacebookServiceName = "FacebookService";
        internal const string GameServiceName = "GameService";
        internal static UnityContainer iocContainer;

        public static UnityContainer ServiceContainer
        {
            get
            {
                return iocContainer;
            }
        }

        static ServiceBroker()
        {
            iocContainer = new UnityContainer();
        }

        internal static void RegisterFacebookService()
        {
            iocContainer.RegisterType<IFacebookService, FacebookService>(ServiceBroker.FacebookServiceName, new ContainerControlledLifetimeManager());
            iocContainer.RegisterType<IGameService, GameService>(ServiceBroker.GameServiceName, new ContainerControlledLifetimeManager());
        }
    }
}
