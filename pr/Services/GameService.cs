﻿using Facebook;
using Microsoft.Owin.Security.Facebook;
using PR.Controllers;
using PR.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PR.Services
{
    public class GameService : IGameService
    {
        GamesController gamesController = new GamesController();
        FacebookUsersController facebookUsersController = new FacebookUsersController();

        public Game GetGame(string email)
        {
            FacebookUser fbUser = facebookUsersController.GetFacebookUserByEmail(email);
            if (fbUser != null)
            {
                return gamesController.GetGamebyFacebookId(fbUser.FacebookId);
            }
            else
            {
                return null;
            }
        }

        public Game CreateGame(string email)
        {
            FacebookUser fbUser = facebookUsersController.GetFacebookUserByEmail(email);
            if (fbUser != null)
            {
                Game game = new Game();
                game.FacebookId = fbUser.FacebookId;
                game.NumberOfRankingsSubmitted = 0;
                return game;
            }
            else
            {
                return null;
            }
        }

        public List<Ranking> GetRankings(Game game)
        {
            List<Ranking> rankings = gamesController.GetRankings(game);
            return rankings;
        }
    }

    internal interface IGameService
    {
        Game GetGame(string email);

        Game CreateGame(string email);

        List<Ranking> GetRankings(Game game);
    }
}