﻿using Facebook;
using Microsoft.Owin.Security.Facebook;
using PR.Controllers;
using PR.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PR.Services
{
    public class FacebookService : IFacebookService
    {
        FacebookUsersController facebookUsersController = new FacebookUsersController();

        public FacebookClient GetFacebookClient(string accessToken)
        {
            FacebookClient client = new FacebookClient(accessToken);
            return client;
        }

        public FacebookUser GetAuthenticatedUser(FacebookAuthenticatedContext context, FacebookClient client)
        {
            /*
            dynamic authUserJson = client.Get("/me?fields=id,first_name,last_name,gender,link,picture,updated_time");
            FacebookUser authUser = new FacebookUser();
            authUser.FacebookId = authUserJson.id;
            authUser.FirstName = authUserJson.first_name;
            authUser.LastName = authUserJson.last_name;
            authUser.Email = context.Email;
            authUser.Gender = authUserJson.gender;
            authUser.Link = authUserJson.link;
            authUser.UpdatedTime = DateTime.Parse(authUserJson.updated_time);
            authUser.Picture = authUserJson.picture.data.url;
            authUser.LoggedIn = true;
            */

            FacebookUser authUser = new FacebookUser();
            authUser.FacebookId = long.Parse(context.User["id"].ToString());
            authUser.FacebookUserName = context.User["username"].ToString();
            authUser.FirstName = context.User["first_name"].ToString();
            authUser.LastName = context.User["last_name"].ToString();
            authUser.Email = context.User["email"].ToString();
            authUser.Gender = context.User["gender"].ToString();
            authUser.UpdatedTime = DateTime.Parse(context.User["updated_time"].ToString());
            authUser.LoggedIn = true;

            return authUser;
        }

        public FacebookUser GetUserByFacebookId(long facebookId)
        {
            FacebookUser fbUser = facebookUsersController.GetFacebookUserByFacebookId(facebookId);
            return fbUser;
        }

        public List<FacebookUser> GetFacebookFriends(FacebookClient client)
        {
            List<FacebookUser> fbFriends = new List<FacebookUser>();
            dynamic friends = client.Get("/me/friends?fields=id,username,first_name,last_name,gender,updated_time");
            foreach (var friend in friends.data)
            {
                FacebookUser fbUser = new FacebookUser();
                fbUser.FacebookId = long.Parse(friend.id);
                fbUser.FacebookUserName = friend.username;
                fbUser.FirstName = friend.first_name;
                fbUser.LastName = friend.last_name;
                fbUser.Email = "";
                fbUser.Gender = friend.gender;
                fbUser.UpdatedTime = DateTime.Parse(friend.updated_time);
                fbUser.LoggedIn = false;
                fbFriends.Add(fbUser);
            }
            return fbFriends;
        }

        public void StoreFacebookUsers(List<FacebookUser> facebookUsers)
        {
            facebookUsersController.StoreFacebookUsers(facebookUsers);
        }

        public void UpdateFacebookUser(FacebookUser facebookUser)
        {
            facebookUsersController.UpdateFacebookUser(facebookUser);
        }

        public void SendFacebookNotification(FacebookClient client, FacebookUser facebookUser, string template, string href)
        {
            dynamic nparams = new ExpandoObject();
            nparams.access_token = "507934389270661|X89UqY_TWFwYOTGUSuiVHrOKEjk";
            nparams.template = template;
            nparams.href = href;
            dynamic notificationResult = client.Post("/" + facebookUser.Id + "/notifications", nparams);
        }
    }

    internal interface IFacebookService
    {
        FacebookClient GetFacebookClient(string accessToken);

        FacebookUser GetAuthenticatedUser(FacebookAuthenticatedContext context, FacebookClient client);

        FacebookUser GetUserByFacebookId(long facebookId);

        List<FacebookUser> GetFacebookFriends(FacebookClient client);

        void StoreFacebookUsers(List<FacebookUser> facebookUsers);

        void UpdateFacebookUser(FacebookUser facebookUser);

        void SendFacebookNotification(FacebookClient client, FacebookUser facebookUser, string template, string href);
    }
}