﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR.Messages
{
    public enum GameMessage
    {
        IsGameCreated,
        CreateNewGame,
        GetUnrankedFriends,
        GetRankings
    }

    public class GameActorMessage
    {
        private HttpContext context;
        private GameMessage message;
        private object data;

        public HttpContext Context { get { return context; } }

        public GameMessage Message { get { return message; } }

        public object Data { get { return data; } }

        public GameActorMessage(HttpContext context, GameMessage message, object data)
        {
            this.context = context;
            this.message = message;
            this.data = data;
        }
    }
}