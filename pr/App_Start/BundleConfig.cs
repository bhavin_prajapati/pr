﻿using System.Web;
using System.Web.Optimization;

namespace PR
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            string cndHost = "cdn";

            var jquery = new AzureScriptBundle("~/bundles/jquery", cndHost).Include(
                        "~/Scripts/jquery-1.11.1.js");
            jquery.Transforms.Add(new JsMinify());
            bundles.Add(jquery);

            var jqueryval = new AzureScriptBundle("~/bundles/jqueryval", cndHost).Include(
                        "~/Scripts/jquery.validate*");
            jqueryval.Transforms.Add(new JsMinify());
            bundles.Add(jqueryval);

            var jqueryplugins = new AzureScriptBundle("~/bundles/jqueryplugins", cndHost).Include(
                "~/Scripts/jquery.bootstrap-duallistbox.js");
            jqueryplugins.Transforms.Add(new JsMinify());
            bundles.Add(jqueryplugins);

            var underscore = new AzureScriptBundle("~/bundles/underscore", cndHost).Include(
                        "~/Scripts/underscore-min.js");
            bundles.Add(underscore);

            var modernizr = new AzureScriptBundle("~/bundles/modernizr", cndHost).Include(
                        "~/Scripts/modernizr-*");
            modernizr.Transforms.Add(new JsMinify());
            bundles.Add(modernizr);

            var bootstrap = new AzureScriptBundle("~/bundles/bootstrap", cndHost).Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js");
            bootstrap.Transforms.Add(new JsMinify());
            bundles.Add(bootstrap);

            var angular = new AzureScriptBundle("~/bundles/angular", cndHost).Include(
                        "~/Scripts/angular.js");
            angular.Transforms.Add(new JsMinify());
            bundles.Add(angular);

            var angular_app = new AzureScriptBundle("~/bundles/angular_app", cndHost).Include(
                        "~/Scripts/angular/app.js");
            angular_app.Transforms.Add(new JsMinify());
            bundles.Add(angular_app);

            var angular_main = new AzureScriptBundle("~/bundles/angular_main", cndHost).Include(
                        "~/Scripts/angular/main.ctrl.js");
            angular_main.Transforms.Add(new JsMinify());
            bundles.Add(angular_main);

            var cssBundle = new AzureStyleBundle("~/Content/css", cndHost).Include(
                        "~/Content/bootstrap.css",
                        "~/Content/style.css",
                        "~/Content/font-awesome.css",
                        "~/Content/bootstrap-duallistbox.css");
            cssBundle.Transforms.Add(new CssMinify());
            bundles.Add(cssBundle);
        }
    }
}
