﻿using Akka.Actor;
using Akka.Event;
using Facebook;
using Microsoft.Owin.Security.Facebook;
using PR.Controllers;
using PR.Models;
using PR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR.Actors
{
    public class FacebookLoginActor: ReceiveActor
    {
        private LoggingAdapter log = Logging.GetLogger(Context);

        private FacebookAuthenticatedContext facebookContext;

        private IFacebookService facebookService;

        public FacebookLoginActor()
        {
            this.facebookService = ServiceBroker.ServiceContainer.Resolve(typeof(IFacebookService), ServiceBroker.FacebookServiceName) as FacebookService;

            Receive<FacebookAuthenticatedContext>(context => 
            {
                try
                {
                    facebookContext = context;
                    if (context.AccessToken != null)
                    {
                        //Creates FacebookClient using token
                        FacebookClient client = facebookService.GetFacebookClient(context.AccessToken);

                        //Gets the authenticated user info from facebook
                        FacebookUser authUser = facebookService.GetAuthenticatedUser(context, client);

                        //Gets information about this authenticated user from database
                        FacebookUser fbUser = facebookService.GetUserByFacebookId(authUser.FacebookId);

                        if (fbUser == null) //New user
                        {
                            //Get that user's friends from facebook
                            List<FacebookUser> friends = facebookService.GetFacebookFriends(client);

                            //Set login count to 1
                            authUser.LoginCount = 1;

                            //Adds user into list
                            friends.Add(authUser);

                            //Stores whole list of users in database
                            facebookService.StoreFacebookUsers(friends);
                        }
                        else //Existing user in db
                        {
                            if (!fbUser.LoggedIn) //First time logging in
                            {
                                //Updates fbUser
                                fbUser.LoginCount = 1;
                                fbUser.Email = context.Email;
                                fbUser.LoggedIn = true;

                                //Update the facebook user in database
                                facebookService.UpdateFacebookUser(fbUser);

                                //Get that user's friends from facebook
                                List<FacebookUser> friends = facebookService.GetFacebookFriends(client);

                                //Stores whole list of users in db
                                facebookService.StoreFacebookUsers(friends);
                            }
                            else if (fbUser.LoginCount % 10 == 0) //Updates friends every 10th login
                            {
                                //Increments login counter
                                fbUser.LoginCount++;

                                //Update the facebook user in database
                                facebookService.UpdateFacebookUser(authUser);

                                //Get that user's friends from facebook
                                List<FacebookUser> friends = facebookService.GetFacebookFriends(client);

                                //Stores whole list of users in db
                                facebookService.StoreFacebookUsers(friends);
                            }
                            else //Increments login count
                            {
                                fbUser.LoginCount++;
                                facebookService.UpdateFacebookUser(fbUser);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    this.Unhandled(e.Message);
                }
            });
        }
    }
}