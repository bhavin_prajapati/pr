﻿using Akka.Actor;
using Akka.Event;
using Facebook;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using PR.Controllers;
using PR.Messages;
using PR.Models;
using PR.Services;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace PR.Actors
{
    public class GameActor: ReceiveActor
    {
        private LoggingAdapter log = Logging.GetLogger(Context);

        private IGameService gameService;

        private IFacebookService facebookService;

        private JavaScriptSerializer jsonSerialiser = new JavaScriptSerializer();

        public GameActor()
        {
            this.gameService = ServiceBroker.ServiceContainer.Resolve(typeof(IGameService), ServiceBroker.GameServiceName) as GameService;
            this.facebookService = ServiceBroker.ServiceContainer.Resolve(typeof(IFacebookService), ServiceBroker.FacebookServiceName) as FacebookService;

            Receive<GameActorMessage>(gam => 
            {
                switch ((GameMessage)gam.Message)
                {
                    case GameMessage.IsGameCreated:
                        try
                        {
                            Game game = gameService.GetGame(gam.Context.User.Identity.Name);
                            bool response = (game != null) ? true : false;
                            Sender.Tell(response);
                        }
                        catch (Exception e)
                        {
                            Sender.Tell(false);
                        }
                        break;
                    case GameMessage.CreateNewGame:
                        ApplicationDbContext db = gam.Data as ApplicationDbContext;
                        Game newGame = gameService.CreateGame(gam.Context.User.Identity.Name);
                        db.Games.Add(newGame);
                        db.SaveChanges();
                        Sender.Tell(newGame);
                        break;
                    case GameMessage.GetUnrankedFriends:
                        try
                        {
                            string accessToken = gam.Data as string;
                            FacebookClient fbClient = facebookService.GetFacebookClient(accessToken);
                            List<FacebookUser> fbUsers = facebookService.GetFacebookFriends(fbClient);
                            var friends = new List<dynamic>();
                            foreach (FacebookUser fbUser in fbUsers)
                            {
                                Dictionary<string, string> friend = new Dictionary<string, string>();
                                friend["name"] = fbUser.FirstName + " " + fbUser.LastName;
                                friend["facebookId"] = fbUser.FacebookId.ToString();
                                friends.Add(friend);
                            }
                            var json = jsonSerialiser.Serialize(friends);
                            Sender.Tell(json);
                        }
                        catch (Exception e)
                        {
                            Sender.Tell("");
                        }
                        break;
                    case GameMessage.GetRankings:
                        Game g = gameService.GetGame(gam.Context.User.Identity.Name);
                        List<Ranking> rankings = gameService.GetRankings(g);
                        var rankingsJson = jsonSerialiser.Serialize(rankings);
                        Sender.Tell(rankingsJson);
                        break;
                }
            });
        }
    }
}