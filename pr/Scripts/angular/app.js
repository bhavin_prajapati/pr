﻿var app = angular.module('app', []);

var FB; // to avoid error "undeclared variable", until FB got initialized
var myQueue = new Array();
function queueAdd(f) {
    if (FB == undefined)
        myQueue.push(f);
    else
        f();
}
function processQueue() {
    var f;
    while (f = myQueue.shift())
        f();
}
