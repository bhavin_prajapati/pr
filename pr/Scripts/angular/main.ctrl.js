﻿app.controller("GameController", function ($scope, $http, friends, accessToken, rankings) {
    var vm = this;
    $scope.friends = friends;
    $scope.accessToken = accessToken;
    $scope.rankings = rankings;
    $scope.currentPage = 0;
    $scope.pageSize = 12;
    $scope.data = [];
    $scope.numberOfPages = function () {
        return Math.ceil($scope.data.length / $scope.pageSize);
    }
    queueAdd(function () {
        $(document).ready(function () {
            _.each(friends, function (friend) {
                FB.api(
                    '/' + friend.facebookId + '/picture?type=large',
                    'get',
                    {
                        access_token: accessToken
                    },
                function (response) {
                    if (!response || response.error) {

                    }
                    else {
                        var item = { "facebookId": friend.facebookId, "name": friend.name, "url": response.data.url };
                        $scope.$apply(function () {
                            $scope.data.push(item);
                        });
                    }
                }
            );
            });
        });
    });
    $scope.ranking = function (index, id, facebookId, rank) {
        $("#rank-" + index).bootstrapDualListbox({
            showFilterInputs: false,
            nonSelectedListLabel: 'Non-selected',
            selectedListLabel: 'Selected',
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true
        });

        $http.post('/api/Ranking', { "SenderId": id, "ReceiverId": facebookId, "TraitId": rank }).
          success(function (data, status, headers, config) {
              // this callback will be called asynchronously
              // when the response is available
          }).
          error(function (data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
          });
    };
});

//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
