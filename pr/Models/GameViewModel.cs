﻿using Akka.Actor;
using Facebook;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using PR.Actors;
using PR.Messages;
using PR.Resources;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace PR.Models
{
    public class GameViewModel
    {
        private FacebookAuthenticatedContext context;
        private string accessToken;
        private ActorRef target;
        private Inbox inbox;

        public GameViewModel()
        {
            target = Publicranks.ActorSystem.ActorOf(Props.Create<GameActor>());
            inbox = Inbox.Create(Publicranks.ActorSystem);
        }
        
        public bool IsGameCreated()
        {
            context = HttpContext.Current.Session[AppConstants.FacebookAuthenticatedContext] as FacebookAuthenticatedContext;
            accessToken = HttpContext.Current.Session[AppConstants.AccessToken] as string;
            GameActorMessage gam = new GameActorMessage(HttpContext.Current, GameMessage.IsGameCreated, null);
            bool isGameCreated = false;
            inbox.Send(target, gam);
            isGameCreated = (bool)inbox.Receive(Publicranks.Timeout);
            return isGameCreated;
        }

        public Game CreateNewGame(ApplicationDbContext db)
        {
            GameActorMessage gam = new GameActorMessage(HttpContext.Current, GameMessage.CreateNewGame, db);
            inbox.Send(target, gam);
            var game = (Game)inbox.Receive(Publicranks.Timeout);
            return game;
        }

        public string GetFriendsList()
        {
            context = HttpContext.Current.Session[AppConstants.FacebookAuthenticatedContext] as FacebookAuthenticatedContext;
            accessToken = HttpContext.Current.Session[AppConstants.AccessToken] as string;
            GameActorMessage gam = new GameActorMessage(HttpContext.Current, GameMessage.GetUnrankedFriends, accessToken);
            inbox.Send(target, gam);
            string friends = (string)inbox.Receive(Publicranks.Timeout);
            return friends;
        }

        public string GetRankings()
        {
            GameActorMessage gam = new GameActorMessage(HttpContext.Current, GameMessage.GetRankings, null);
            inbox.Send(target, gam);
            string rankings = (string)inbox.Receive(Publicranks.Timeout);
            return rankings;
        }

        public string GetAccessToken()
        {
            return accessToken;
        }

        public string GetFacebookId()
        {
            if (context != null)
            {
                return context.Id;
            }
            else
            {
                return "";
            }
        }
    }
}