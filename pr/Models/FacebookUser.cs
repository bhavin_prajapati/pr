﻿using Facebook;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace PR.Models
{
    public class FacebookUser
    {
        public long Id { get; set; }

        public long FacebookId { get; set; }

        public string FacebookUserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Gender { get; set; }

        public DateTime UpdatedTime { get; set; }
        
        public bool LoggedIn { get; set; }

        public int LoginCount { get; set; }
    }
}