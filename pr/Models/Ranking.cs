﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR.Models
{
    public class Ranking
    {
        public long Id { get; set; }

        public Game Game { get; set; }

        public long SenderId { get; set; }

        public long ReceiverId { get; set; }

        public Trait TraitId { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}