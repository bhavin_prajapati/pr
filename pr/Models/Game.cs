﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR.Models
{
    public class Game
    {
        public long Id { get; set; }

        public long FacebookId { get; set; }

        public int NumberOfRankingsSubmitted { get; set; }
    }
}