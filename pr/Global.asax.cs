﻿using Akka.Actor;
using Microsoft.WindowsAzure.ServiceRuntime;
using PR.Models;
using PR.Resources;
using PR.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PR
{
    public class Publicranks : System.Web.HttpApplication
    {
        private static ActorSystem actorSystem;
        private static TimeSpan timeout = new TimeSpan(0, 1, 30);

        public static ActorSystem ActorSystem {
            get { return actorSystem; }
        }

        public static TimeSpan Timeout
        {
            get { return timeout; }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            if (RoleEnvironment.IsAvailable)
            {
#if DEBUG
                BundleTable.EnableOptimizations = false;
                BundleTable.Bundles.UseCdn = false;
#else
                BundleTable.EnableOptimizations = true;
                BundleTable.Bundles.UseCdn = true;
#endif
            }
            Database.SetInitializer<ApplicationDbContext>(null);
            actorSystem = ActorSystem.Create(AppConstants.PRActorSystem);
            ServiceBroker.RegisterFacebookService();
        }
    }
}
